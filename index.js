// Module imports
const relate = require('./lib/relate')
const store = require('./lib/store')
const validate = require('./lib/validate')

// Default module exports our MongoDB connector
module.exports = store

// Module also exports a helper function to assign relationships to user input
module.exports.relate = relate

// Module also exports some model validation utilities
module.exports.validate = validate

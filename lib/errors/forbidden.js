// Module imports
const BadRequest = require('./bad-request')

// Module exports a HTTP 403 like message for modifying UUIDs that don't belong to related users
module.exports = class Forbidden extends BadRequest {
  constructor (model, input) {
    super(model, '403 Forbidden ' + input.id)
  }
}

// Module exports a HTTP 400 like error message for providing invalid request data
module.exports = class BadRequest extends Error {
  constructor (model, message) {
    super(`Invalid ${model.class}: ${message || '400 Bad Request'}`)
  }
}

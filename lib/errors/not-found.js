// Module imports
const BadRequest = require('./bad-request')

// Module exports a HTTP 404 like message for querying UUIDs that don't exist
module.exports = class NotFound extends BadRequest {
  constructor (model, input) {
    super(model, '404 Not Found ' + input.id)
  }
}

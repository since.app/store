/**
 * Assigns a relation to the given user input.
 *
 * @function                relate
 * @param    {Array|Object} input    Input to relate
 * @param    {Object}       relation Relationships to assign
 * @returns  {Array|Object}          Input data with assigned relations
 */
module.exports = function relate (input, relation) {
  // Helper function that actually assigns the relationship to the user input
  const relates = (item) => Object.assign(item, relation)

  // Return the mapped data or object with the new relation
  return Array.isArray(input) ? input.map(relates) : relates(input)
}

// Module imports
const { BadRequest, Forbidden, NotFound } = require('./errors')

// Module exports validation function for interacting with our MongoDB store
module.exports = {
  belongs,
  exists
}

/**
 * Returns a model ownership validator that will throw an error if invalid.
 *
 * @returns {Function} Validator function
 */
function belongs (user) {
  /**
   * Validates the ownership of the model requested.
   *
   * @param  {Model}    model  Mongoose static model
   * @param  {Object}   input  User input related to queried model
   * @param  {Model}    record The model the user is querying
   * @throws {NotFound}
   */
  return function validate (model, input, record) {
    // Make sure the timestamp belongs to the user
    if (record && record.user && record.user !== user) throw new Forbidden(model, input)
  }
}

/**
 * Returns a model existence validator that will throw an error if invalid.
 *
 * @param   {Boolean}  duplicate Throw an error if the model is found
 * @returns {Function}
 */
function exists (duplicate) {
  /**
   * Validates the existence of the model requested.
   *
   * @param  {Model}    model  Mongoose static model
   * @param  {Object}   input  User input related to queried model
   * @param  {Model}    record The model the user is querying
   * @throws {NotFound}
   */
  return function validate (model, input, record) {
    // Default behaviour is to check if a UUID exists
    if (!duplicate && !record) throw new NotFound(model, input)

    // Flipped behaviour checks if a duplicate UUID exists
    if (duplicate && record) throw new BadRequest(model, 'Duplicate Found ' + input.id)
  }
}

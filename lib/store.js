/**
 * MongoDB wrapper.
 *
 * Declaring Mongoose as a package dependency and attempting to connect using it as an
 * import won't work. The connection will be established but Mongoose won't know
 * about the models each project uses.
 *
 * @param   {String} url      MongoDB connection string
 * @param   {Object} mongoose Mongoose dependency
 * @returns {Object}          CRUD operations
 */
module.exports = class Store {
  constructor (url, mongoose) {
    /**
     * API connection status.
     *
     * @type {Boolean}
     */
    this.online = undefined

    /**
     * Mongoose connection.
     *
     * @type {Connection}
     */
    const db = mongoose.connection

    /**
     * MongoDB connection options.
     *
     * @type {Object}
     */
    const opts = {
      useFindAndModify: false,
      useNewUrlParser: true,
      useUnifiedTopology: true
    }

    /**
     * Set connection error and prevent further calls.
     *
     * @type {Closure}
     */
    const closed = () => { this.online = false }

    /**
     * Clears connection failure.
     *
     * @type {Closure}
     */
    const opened = () => { this.online = true }

    // Set connection event handlers
    db.on('error', closed)
    db.on('connected', opened)
    db.on('reconnected', opened)

    // Immediately attempt to connect to our database
    mongoose.connect(url, opts).then(opened, closed)
  }

  /**
   * Saves a record to our MongoDB connection.
   *
   * @param   {Model}         model Mongoose static model
   * @param   {Object}        input Model hydration data
   * @returns {Query|Promise}
   */
  add (model, input) {
    // Rejected promise with DB connection error
    if (!this.online) return this.offline()

    if (Array.isArray(input)) {
      // Save multiple records and return them
      return model.insertMany(input.map(this.prepare))
    } else {
      // Save a single record and return it
      return model.create(this.prepare(input))
    }
  }

  /**
   * Checks if a record exists.
   *
   * @param   {Model}         model  Mongoose static model
   * @param   {Object}        filter MongoDB find filter
   * @returns {Query|Promise}
   */
  exists (model, filter) {
    return this.online ? model.exists(filter) : this.offline()
  }

  /**
   * Finds multiple records by filter.
   *
   * @param   {Model}         model  Mongoose static model
   * @param   {Object}        filter MongoDB find filter
   * @returns {Query|Promise}
   */
  filter (model, filter) {
    return this.online ? model.find(filter) : this.offline()
  }

  /**
   * Finds a single record by filter.
   *
   * @param   {Model}         model  Mongoose static model
   * @param   {Object}        filter MongoDB find filter
   * @returns {Query|Promise}
   */
  find (model, filter) {
    return this.online ? model.findOne(filter) : this.offline()
  }

  /**
   * Finds multiple records by ID.
   *
   * @param   {Model}         model Mongoose static model
   * @param   {Array}         ids   List of UUIDv4 to find
   * @returns {Query|Promise}
   */
  many (model, ids) {
    return this.filter(model, { _id: { $in: ids } })
  }

  /**
   * Promise to return when database connection fails.
   *
   * @type {Closure}
   * @returns {Promise}
   */
  offline () {
    return Promise.reject(new Error('Database offline'))
  }

  /**
   * Finds a single record by ID.
   *
   * @param   {Model}         model Mongoose static model
   * @param   {String}        id    UUIDv4
   * @returns {Query|Promise}
   */
  one (model, id) {
    return this.find(model, { _id: id })
  }

  /**
   * Helper function that prepares data ID properties for use with MongoDB.
   *
   * @param   {Object} data Data to modify
   * @returns {Object}      Modified data
   */
  prepare (data) {
    // Not specifying an ID means Mongoose needs to generate one
    if (!data.id) return data

    // All application layers use { id: uuid } except for Mongo which uses { _id: uuid }
    Object.assign(data, { _id: data.id })

    // Return the input data with the corrected ID property
    return delete data.id && data
  }

  /**
   * Deletes one or many records by ID.
   *
   * @param   {Model}         model Mongoose static model
   * @param   {String[]}      id    UUIDv4
   * @returns {Query|Promise}
   */
  remove (model, id) {
    // Rejected promise with DB connection error
    if (!this.online) return this.offline()

    // Query to find and delete multiple records by ID
    const filter = { _id: { $in: id } }

    if (Array.isArray(id)) {
      // Deleting and returning multiple records doesn't work natively in MongoDB
      return new Promise((resolve, reject) => {
        model.find(filter).then((records) => {
          model.deleteMany(filter).then(() => {
            resolve(records)
          }, reject)
        }, reject)
      })
    } else {
      // Delete single record
      return model.findByIdAndDelete(id)
    }
  }

  /**
   * Updates one or many records by ID.
   *
   * @param   {Model}         model Mongoose static model
   * @param   {Object}        input Updated data
   * @returns {Query|Promise}
   */
  update (model, input) {
    // Rejected promise with DB connection error
    if (!this.online) return this.offline()

    // Forces update to return the updated record instead of the old one
    const opts = { new: true }

    if (Array.isArray(input)) {
      // Update multiple records and return the newly updated record
      return Promise.all(input.map((data) => model.findByIdAndUpdate(data.id, data, opts)))
    } else {
      // Update a single record and return it
      return model.findByIdAndUpdate(input.id, input, opts)
    }
  }

  /**
   * Attempts to validate each model UUID.
   *
   * @param   {Model}         model        Mongoose static model
   * @param   {Array}         input        List of models to validate
   * @param   {Array}         validators   List of validators to run
   * @returns {Query|Promise}
   * @throws  {Error}
   */
  validate (model, input, validators) {
    // Convert single object requests into arrays
    if (!Array.isArray(input)) input = [input]
    if (!Array.isArray(validators)) validators = [validators]

    // Fetch the models the user is attempting to validate
    return this.many(model, input.map((item) => item.id || item)).then((results) => {
      const records = {}

      // Convert the array of records into an object of UUID key value pairs
      results.forEach((item) => { records[item.id] = item })

      // Run all of the validation rules on each requested model
      input.forEach((item) => {
        // Validators throw errors when invalid
        validators.forEach((validate) => validate(model, item, records[item.id || item]))
      })
    })
  }
}
